#include "Board.h"

Board::Board(std::string board, bool startingColor) : _isPlayerColorWhite(startingColor)
{
	this->_board = Comm::translateBoard(board);
	this->_lastEatenPiece = EMPTY_SQUARE;
}

int Board::resolveTurn(std::string step)
{
	Piece* piece = nullptr;
	std::vector<Piece*> pieceTypes = { new Bishop(), new Rook(), new Queen()};
	std::vector<int> coords;

	//Check if the coord are valid, if not then there is error number 5.
	if (!validCoords(step))
	{
		return 5;
	}

	coords = Comm::translateStep(step, this->_isPlayerColorWhite);
	
	int errorCode = this->switchCaseOfletters(&piece, coords); // Returns 2 if there's no piece in chosen square.
	if (errorCode == 2)
	{
		// Piece pointer is empty, no need to free.
		return 2;
	}
	else
	{
		errorCode = validateMove(coords);
		if (errorCode != 0)
		{
			return errorCode;
		}
	}

	// If the move isn't valid for the moved piece.
	if (!piece->validStep(coords, this->_board))
	{
		return 6;
	}

	// In case of mate, AKA the target square has the king and the move has passed all tests (shouldn't happen because that
	// means that the king moved an illegal move and stayed in check for a turn but in case of a bug, it's a feature :))
	if (this->_isPlayerColorWhite && this->_board[NEW_SQUARE] == 'k')
	{
		return 8;
	}
	else if(!this->_isPlayerColorWhite && this->_board[NEW_SQUARE] == 'K')
	{
		return 8;
	}

	// Checks if the move is legal in terms of is the king getting checked because of this move.
	this->updateBoard(coords);
	if (King::checkIfChecked(coords, _board))
	{
		this->revertBoard(coords);
		return 4;
	}

	// Checks if the moving piece does a check on the king.
	// Known bug: If one player moves a piece that reveals a check on the king, it won't count as a check.
	if (piece->checkIfInPath(coords, this->_board))
	{
		errorCode = 1;
	}
	//Change the color to the one of the next player.
	this->_isPlayerColorWhite = !this->_isPlayerColorWhite;

	free(piece);
	freePtrVector(pieceTypes);
	piece = nullptr;
	return errorCode;
}

int Board::validateMove(const std::vector<int>& coords) const
{
	int errorCode = 0;

	//The player color is black and the chosen piece to move is capital letter.
	if (coords[PLAYER_COLOR] == IS_BLACK && this->_board[LAST_SQUARE] < 'a')
	{
		errorCode = 2;
	}
	//The player color is white and the chosen piece to move is a small letter.
	else if (coords[PLAYER_COLOR] == IS_WHITE && this->_board[LAST_SQUARE] >= 'a')
	{
		errorCode = 2;
	}

	//The player color is black and on the chosen square there is a small letter.
	if (coords[PLAYER_COLOR] == IS_BLACK && this->_board[NEW_SQUARE] >= 'a')
	{
		errorCode = 3;
	}
	//The player color is white and on the chosen square there is a capital letter.
	else if (coords[PLAYER_COLOR] == IS_WHITE && this->_board[NEW_SQUARE] < 'a' && 
		this->_board[coords[Y_NEW_LOC]][coords[X_NEW_LOC]] != EMPTY_SQUARE)
	{
		errorCode = 3;
	}

	//The last square and the new square are the same.
	if (coords[X_NEW_LOC] == coords[X_LAST_LOC] && coords[Y_NEW_LOC] == coords[Y_LAST_LOC])
	{
		errorCode = 7;
	}

	return errorCode;
}

int Board::switchCaseOfletters(Piece** piece, const std::vector<int>& coords) const
{
	int errorCode = 0;
	switch (this->_board[coords[Y_LAST_LOC]][coords[X_LAST_LOC]])
	{
	case 'K':
		*piece = new King();
		break;
	case 'k':
		*piece = new King();
		break;
	case 'R':
		*piece = new Rook();
		break;
	case 'r':
		*piece = new Rook();
		break;
	case 'P':
		*piece = new Pawn();
		break;
	case 'p':
		*piece = new Pawn();
		break;
	case 'Q':
		*piece = new Queen();
		break;
	case 'q':
		*piece = new Queen();
		break;
	case 'B':
		*piece = new Bishop();
		break;
	case 'b':
		*piece = new Bishop();
		break;
	case 'N':
		*piece = new Knight();
		break;
	case 'n':
		*piece = new Knight();
		break;
	case EMPTY_SQUARE:
		errorCode = 2;
	}
	return errorCode;
}

void Board::updateBoard(const std::vector<int>& coords)
{
	this->_lastEatenPiece = this->_board[NEW_SQUARE];
	this->_board[NEW_SQUARE] = this->_board[LAST_SQUARE];
	this->_board[LAST_SQUARE] = EMPTY_SQUARE;
}

void Board::revertBoard(const std::vector<int>& coords)
{
	this->_board[LAST_SQUARE] = this->_board[NEW_SQUARE];
	this->_board[NEW_SQUARE] = this->_lastEatenPiece;
}

bool Board::validCoords(const std::string step)
{
	bool validCoords = false;
	int i = 0;

	for (i = 0; i < COORDS; i++)
	{
		//If i is even - letter, else - number
		if (i % 2 == 0)
		{
			if (step[i] >= 'a' && step[i] <= 'h')
			{
				validCoords = true;
			}
		}
		else
		{
			if (step[i] >= 1 && step[i] <= 8)
			{
				validCoords = true;
			}
		}
	}

	return validCoords;
}

void Board::printBoard() const
{
	int i = 0;
	int j = 0;

	std::cout << "  a b c d e f g h" << std::endl;
	//for (i = 0; i < BOARD_EDGE; i++)
	for (i = BOARD_EDGE - 1; i >= 0; i--)
	{
		//std::cout << BOARD_EDGE - i << ' ';
		std::cout << i + 1 << ' ';
		for (j = 0; j < BOARD_EDGE; j++)
		{
			std::cout << this->_board[i][j] << ' ';
		}
		std::cout << std::endl;
	}
}

std::vector<std::vector<char>> Board::getBoard() const
{
	return this->_board;
}

bool Board::isPlayerColorWhite() const
{
	return this->_isPlayerColorWhite;
}
