/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/
#include "Pipe.h"
#include "Board.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	
	string boardString = "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR";
	string msgFromGraphics;
	Board board = Board(boardString);
	int errorCode = 0;

	strcpy_s(msgToGraphics, "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0"); // just example...
	

	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics

	board.printBoard();

	while (msgFromGraphics != "quit")
	{

		// get message from graphics
		string msgFromGraphics = p.getMessageFromGraphics();

		// Resolves the turn and returns the errorCode.
		errorCode = board.resolveTurn(msgFromGraphics);
		std::cout << "error code:" << errorCode << std::endl;

		int color = board.isPlayerColorWhite() ? 0 : 1;

		msgToGraphics[0] = (char)(errorCode + '0');
		msgToGraphics[1] = 0;

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		

		board.printBoard();

	}

	p.close();
}