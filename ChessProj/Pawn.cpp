#include "Pawn.h"

bool Pawn::validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const
{
	bool validStep = false;
	int dir = coords[PLAYER_COLOR] == IS_BLACK ? -1 : 1;

	//Normal step of pawn.
	if (board[coords[Y_NEW_LOC]][coords[X_NEW_LOC]] == EMPTY_SQUARE)
	{
		/*
		Pn - Pawn new
		Pl - Pawn last
		#	Pn	#		or		#	Pl	#
		#	Pl	#				#	Pn	#
		*/
		if (coords[X_LAST_LOC] == coords[X_NEW_LOC] && coords[Y_LAST_LOC] + dir == coords[Y_NEW_LOC])
		{
			validStep = true;
		}
		/*
		#	Pn	#		or		#	Pl	#
		#	#	#				#	#	#
		#   Pl  #				#	Pn	#
		*/
		else if (coords[X_LAST_LOC] == coords[X_NEW_LOC] && coords[Y_NEW_LOC] == coords[Y_LAST_LOC] + 2 * dir && board[coords[Y_LAST_LOC] + dir][coords[X_NEW_LOC]] == EMPTY_SQUARE)
		{
			//If it is the first move of the black Pawn.  
			if (coords[PLAYER_COLOR] == IS_BLACK && coords[Y_LAST_LOC] == PAWN_LINE_BLACK)
			{
				validStep = true;
			}
			//If it is the first move of the white Pawn.
			else if (coords[PLAYER_COLOR] == IS_WHITE && coords[Y_LAST_LOC] == PAWN_LINE_WHITE)
			{
				validStep = true;
			}
		}
	}
	/*
	O - other piece
	#	Pl	#		or		O	#	O
	O	#	O				#	Pl  #
	*/
	else if(board[coords[Y_NEW_LOC]][coords[X_NEW_LOC]] != EMPTY_SQUARE)
	{
		if(coords[Y_NEW_LOC] == coords[Y_LAST_LOC] + dir && coords[X_NEW_LOC] == coords[X_LAST_LOC] + 1)
		{
			validStep = true;
		}
		else if (coords[Y_NEW_LOC] == coords[Y_LAST_LOC] + dir && coords[X_NEW_LOC] == coords[X_LAST_LOC] - 1)
		{
			validStep = true;
		}
	}
	return validStep;
}

bool Pawn::checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece) 
{
	// Changes the piece the function searches for.
	// By default, the function searches for a king. so if the piece is 'k', whice is the 
	// default, it changes the piece to a black king or white king, according to the current turn color.
	// If it's not 'k', then just leave it as the original piece.
	piece = (piece == 'k') ? (coords[PLAYER_COLOR] == IS_WHITE) ? 'k' : 'K' : piece;
	bool check = false;
	int dir = coords[PLAYER_COLOR] == IS_BLACK ? -1 : 1;
	/*
	#	P	#		or		k	#	k
	k	#	k				#	P  #
	*/
	if (coords[Y_NEW_LOC] + dir >= 0 && coords[Y_NEW_LOC] + dir <= BOARD_WIDTH)
	{
		if (coords[X_NEW_LOC] + 1 >= 0 && coords[X_NEW_LOC] + 1 <= BOARD_WIDTH)
		{
			if (board[coords[Y_NEW_LOC] + dir][coords[X_NEW_LOC] + 1] == piece)
			{
				check = true;
			}
		}
		else if (coords[X_NEW_LOC] - 1 >= 0 && coords[X_NEW_LOC] - 1 <= BOARD_WIDTH)
		{
			if (board[coords[Y_NEW_LOC] + dir][coords[X_NEW_LOC] - 1] == piece)
			{
				check = true;
			}
		}
	}
	
	return check;
}

char Pawn::getTypeChr(int currColor)
{
	// If the current player color is white, the piece is uppercase, else it's lowercase
	char piece = (currColor == IS_BLACK) ? 'p' : 'P';
	return piece;
}
