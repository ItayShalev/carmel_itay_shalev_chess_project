#pragma once
#include "Piece.h"
#include "Knight.h"
#include "Bishop.h"
#include "Rook.h"
#include "Queen.h"

class King : public Piece
{
public:
	// Checks if the move is valid based on the King's movement.
	virtual bool validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const;

	// Checks if the piece attacks the given piece char after the move has been made.
	// Defaults to king for checking if there's a check, but is also used for other uses.
	virtual bool checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece = 'k');

	// Checks if the king of the current color (according to the color passed in coords) will be checked if 
	// the move in coords is made.
	// Function is static because it doesn't use a specific king instance, as it understands the color of the king by the coords.
	static bool checkIfChecked(const std::vector<int>& coords, const std::vector<std::vector<char>>& board);

	// Returns the type of the current piece.
	virtual char getTypeChr(int currColor);

	// Gets the coordinates of the king based on the color passed in coords in the form of x, y.
	// Static function because there's no need for an instance of the piece.
	static std::vector<int> getKingCoords(const std::vector<std::vector<char>>& board, bool color);
};
// Used to free a vector of piece pointers.
void freePtrVector(std::vector<Piece*>& vec);

// Checks relation between 2 points in the coords vector (x0, y0, x1, y1).
// 0 - Doesn't have any specific relation.
// 1 - If the 2 points are on the same diagonal line.
// 2 - if the 2 points are on the same strait line.
int checkRelation(std::vector<int>& coords);