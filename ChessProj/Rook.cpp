#include "Rook.h"

bool Rook::validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const
{
	bool validStep = false;
	int checkers = 0;
	int i = 0;

	/*
	#	Rn	#	
	#	Rn	#		
	#	Rl	#	
	#	Rn	#
	#	Rn	#
	*/
	if (coords[X_LAST_LOC] == coords[X_NEW_LOC])
	{
		checkers = coords[Y_LAST_LOC] - coords[Y_NEW_LOC];
		checkers = abs(checkers);
		int upper = coords[Y_LAST_LOC] < coords[Y_NEW_LOC] ? 1 : -1;
		for (i = 1; i < checkers; i++)
		{
			if (board[coords[Y_LAST_LOC] + i * upper][coords[X_NEW_LOC]] != EMPTY_SQUARE)
			{
				break;
			}
		}
		validStep = i == checkers;
	}
	/*
	#	#	#	#	#	#
	Rn	Rn	Rn	Rl	Rn	Rn
	#	#	#	#	#	#
	*/
	else if (coords[Y_LAST_LOC] == coords[Y_NEW_LOC])
	{
		checkers = abs(coords[X_LAST_LOC] - coords[X_NEW_LOC]);
		int right = coords[X_LAST_LOC] < coords[X_NEW_LOC] ? 1 : -1;
		for (i = 1; i < checkers; i++)
		{
			if (board[coords[Y_NEW_LOC]][coords[X_LAST_LOC] + i * right] != EMPTY_SQUARE)
			{
				break;
			}
		}
		validStep = i == checkers;
	}

	return validStep;
}

bool Rook::checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece)
{
	// Changes the piece the function searches for.
	// By default, the function searches for a king. so if the piece is 'k', whice is the 
	// default, it changes the piece to a black king or white king, according to the current turn color.
	// If it's not 'k', then just leave it as the original piece.
	piece = (piece == 'k') ? (coords[PLAYER_COLOR] == IS_WHITE) ? 'k' : 'K' : piece;
	bool check = false;
	int squareSide1 = 0;
	int squareSide2 = 0;
	int emptyChecker = true;
	int i = 0;

	squareSide1 = BOARD_WIDTH - coords[Y_NEW_LOC]; // Number of squares from the rook to the top of the board.
	squareSide2 = coords[Y_NEW_LOC]; // Number of squares from the rook to the bottom of the board.

	/*
	#	k?	#
	#	k?	#
	#	R	#
	*/
	// Checks all squares above the rook if there's a king there.
	for (i = 1; i <= squareSide1; i++)
	{
		// If the rook is on the edge of the board, he won't be able to attack anything.
		if (coords[Y_NEW_LOC] == BOARD_WIDTH)
		{
			break;
		}

		// Checks if the current square has the piece that it looks for.
		if (board[coords[Y_NEW_LOC] + i][coords[X_NEW_LOC]] == piece)
		{
			return true;
			break;
		}
		// If there's a piece in the path, break the loop.
		else if (board[coords[Y_NEW_LOC] + i][coords[X_NEW_LOC]] != EMPTY_SQUARE)
		{
			break;
		}
	}
	/*
	#	R	#
	#	k?	#
	#	k?  #
	*/
	// does the same as above for the squares below the rook.
	for (i = 1; i <= squareSide2; i++)
	{
		// If the rook is on the edge of the board, he won't be able to attack anything.
		if (coords[Y_NEW_LOC] == 0)
		{
			break;
		}
		// Checks if the current square has the piece that it looks for.
		if (board[coords[Y_NEW_LOC] - i][coords[X_NEW_LOC]] == piece)
		{
			return true;
			break;
		}
		// If there's a piece in the path, break the loop.
		else if (board[coords[Y_NEW_LOC] - i][coords[X_NEW_LOC]] != EMPTY_SQUARE)
		{
			break;
		}
	}
	
	squareSide1 = BOARD_WIDTH - coords[X_NEW_LOC];
	squareSide2 = coords[X_NEW_LOC];

	/*
	#	#	#
	R	k?	k?
	#	#	#
	*/
	// does the same as above for the squares right to the rook.
	for (i = 1; i <= squareSide1; i++)
	{
		// If the rook is on the edge of the board, he won't be able to attack anything.
		if (coords[X_NEW_LOC] == BOARD_WIDTH)
		{
			break;
		}
		// Checks if the current square has the piece that it looks for.
		if (board[coords[Y_NEW_LOC]][coords[X_NEW_LOC] + i] == piece)
		{

			return true;
			break;
		}
		// If there's a piece in the path, break the loop.
		else if (board[coords[Y_NEW_LOC]][coords[X_NEW_LOC] + i] != EMPTY_SQUARE)
		{
			break;
		}

	}
	/*
	#	#	#	
	k?	k?	R	
	#	#	#	
	*/
	// does the same as above for the squares left the rook.
	for (i = 1; i <= squareSide2; i++)
	{
		// If the rook is on the edge of the board, he won't be able to attack anything.
		if (coords[X_NEW_LOC] == 0)
		{
			break;
		}
		// Checks if the current square has the piece that it looks for.
		else if (board[coords[Y_NEW_LOC]][coords[X_NEW_LOC] - i] == piece)
		{

			return true;
			break;
		}
		// If there's a piece in the path, break the loop.
		if (board[coords[Y_NEW_LOC]][coords[X_NEW_LOC] - i] != EMPTY_SQUARE)
		{
			break;
		}
	}

	return false;
}

char Rook::getTypeChr(int currColor)
{
	// If the current player color is white, the piece is uppercase, else it's lowercase
	char piece = (currColor == IS_BLACK) ? 'r' : 'R';
	return piece;
}
