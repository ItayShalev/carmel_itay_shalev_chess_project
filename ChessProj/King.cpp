#include "King.h"

bool King::validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const
{
	// If the distance between the coord's new X or new Y and the old X and old Y
	// is bigger than 1, it's an illegal move for the king and the returned value is false
	// If it isn't, than it's a legal move.
	// Split the 'if' statment into 2 just for the tidiness
	if (abs(coords[X_LAST_LOC] - coords[X_NEW_LOC]) > 1)
	{
		return false;
	}
	else if (abs(coords[Y_LAST_LOC] - coords[Y_NEW_LOC]) > 1)
	{
		return false;
	}
	
	return true;
}

bool King::checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece)
{
	int x = -1;
	int  y = -1;
	// Two for loops to cover all the possible combinations of the king's movement.
	for (x = -1; x < 2 && coords[X_NEW_LOC] + x >= 0 && coords[X_NEW_LOC] + x >= BOARD_WIDTH; x++)
	{ 
		for (y = -1; y < 2 && coords[Y_NEW_LOC] + y >= 0 && coords[Y_NEW_LOC] + y >= BOARD_WIDTH; y++)
		{
			if (board[coords[Y_NEW_LOC] + y][coords[X_NEW_LOC] + x] == piece)
			{
				return true;
			}
		}
	}
	return false;
}

// Probebly the most complicated function in this code...
bool King::checkIfChecked(const std::vector<int>& coords, const std::vector<std::vector<char>>& board)
{
	char king = (coords[PLAYER_COLOR] == IS_WHITE) ? 'K' : 'k';
	std::vector<Piece*> pieceTypes = { new Knight(), new Bishop(), new Rook(), new Queen() , new King};
	bool checked = false;

	// If the moved piece is the king
	
	if (board[NEW_SQUARE] == king)
	{
		// cycle through all piece types except the pawn and check if that piece could get to itself, if it was in the king's position.
		// Meaning that in case of the rook, for example, if we were to put a rook instead of the king's new position 
		// (after the step has been made) and that rook could get to another rook from his position, that would mean that
		// there would be a check on the king in the same position.
		for (int i = 0; i < pieceTypes.size(); i++)
		{
			// If the function which checks if a piece is in the path returns true, means that the king will be checked
			// so function should return true.
			// !coords[PLAYER_COLOR] because needs to find piece of oppsite color
			if (pieceTypes[i]->checkIfInPath(coords, board, pieceTypes[i]->getTypeChr(!coords[PLAYER_COLOR])))
			{
				checked = true;
			}
		}

		// For the pawns.
		// If the color is white, looks for a black pawn.
		char pawn = (coords[PLAYER_COLOR] == IS_WHITE) ? 'p' : 'P';
		if (coords[PLAYER_COLOR] == IS_WHITE)
		{
			// If the current color is white, checks the two diagonally placed squares above the king.
			if ((coords[Y_NEW_LOC] + 1 <= BOARD_WIDTH && coords[X_NEW_LOC] - 1 >= 0 && // Validates that indexes are in range.
				board[coords[Y_NEW_LOC] + 1][coords[X_NEW_LOC] - 1] == pawn) ||
				(coords[Y_NEW_LOC] + 1 <= BOARD_WIDTH && coords[X_NEW_LOC] + 1<= BOARD_WIDTH && // Validates that indexes are in range.
					board[coords[Y_NEW_LOC] + 1][coords[X_NEW_LOC] + 1] == pawn))
			{
				checked = true;
			}
		}
		else
		{
			// If the current color is black, checks the two diagonal squares below the king.
			if ((coords[Y_NEW_LOC] - 1 >= 0 && coords[X_NEW_LOC] - 1 <= 0 && // Validates that indexes are in range.
				board[coords[Y_NEW_LOC] - 1][coords[X_NEW_LOC] - 1] == pawn) ||
				(coords[Y_NEW_LOC] - 1 >= 0 && coords[X_NEW_LOC] + 1 >= BOARD_WIDTH && // Validates that indexes are in range.
				board[coords[Y_NEW_LOC] - 1][coords[X_NEW_LOC] + 1] == pawn))
			{
				checked = true;
			}
		}

		
	}
	// If the piece moved is something other then the king, the move was validated before to make sure that there's a piece
	// so no need to check that again.
	else
	{
		// Only the rook, bishop and queen can cause a check if a piece were to move and reveal a path to the king.
		// So searches only for these pieces.
		std::vector<Piece*> piecesToCheck = { new Rook(), new Bishop() , new Queen()};
		char piece = board[NEW_SQUARE]; // The piece that has been moved
		std::vector<int> kingPos = getKingCoords(board, coords[PLAYER_COLOR]);
		std::vector<int> kingCoords = { coords[X_LAST_LOC], coords[Y_LAST_LOC], kingPos[0], kingPos[1],
										coords[PLAYER_COLOR] };

		// If the moved piece is in a strait line with the king.
		if (checkRelation(kingCoords) == 2) // If the moved piece was a rook.
		{
			// The coords like the ones that were passed only instead of new coords given the king's coords, 
			// that is needed because of the function checkIfInPath.
			// The changed coordinates basicallly represent the piece's position, after the move.
			// but that's not what is needed.
			if (piecesToCheck[0]->checkIfInPath(kingCoords, board, piecesToCheck[0]->getTypeChr(!coords[PLAYER_COLOR])) ||
				piecesToCheck[0]->checkIfInPath(kingCoords, board, piecesToCheck[2]->getTypeChr(!coords[PLAYER_COLOR]))) // Searches for the rook or queen
			{
				// if returns true, means there's a path to the king if the move were to happen.
				checked = true;
			}
		}
		// If the moved piece is in a diagonal line with the king.
		else if (checkRelation(kingCoords) == 1)
		{
			// Searches for the bishop or queen.
			if (piecesToCheck[1]->checkIfInPath(kingCoords, board, piecesToCheck[1]->getTypeChr(!coords[PLAYER_COLOR])) ||
				piecesToCheck[1]->checkIfInPath(kingCoords, board, piecesToCheck[2]->getTypeChr(!coords[PLAYER_COLOR]))) 
			{
				// if returns true, means there's a path to the king if the move were to happen.
				checked = true;
			}
		}
	}
	freePtrVector(pieceTypes);
	return checked;
}

char King::getTypeChr(int currColor)
{
	// If the current player color is white, the piece is uppercase, else it's lowercase
	char piece = (currColor == IS_BLACK) ? 'k' : 'K';
	return piece;
}

std::vector<int> King::getKingCoords(const std::vector<std::vector<char>>& board, bool color)
{
	char king = (color == IS_BLACK) ? 'k' : 'K';
	std::vector<int> coords;
	for (int y = 0; y <= BOARD_WIDTH; y++)
	{
		for (int x = 0; x <= BOARD_WIDTH; x++)
		{
			if (board[y][x] == king)
			{
				coords.push_back(x);
				coords.push_back(y);
				return coords;
			}
		}
	}

	return coords;
}

void freePtrVector(std::vector<Piece*>& vec)
{
	for (int i = 0; i < vec.size(); i++)
	{
		delete(vec[i]);
	}
}

int checkRelation(std::vector<int>& coords)
{
	// A line is diagonal if |x0 - x1| = |y0 - y1|
	if (abs(coords[0] - coords[2]) == abs(coords[1] - coords[3]))
	{
		return 1; // 1 - Diagonal line.
	}
	// If the x values or the y values are equal, than the lines are on a strait line.
	else if (coords[0] == coords[2] || coords[1] == coords[3])
	{
		return 2; // 2 - Strait line.
	}
	return 0; // 0 - Doesn't have relation.
}
