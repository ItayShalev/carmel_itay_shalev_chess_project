#pragma once
#include "Piece.h"


class Bishop : public Piece
{
public:
	// Checks if the move is valid based on the Bishop's movement.
	virtual bool validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const;

	// Checks if the piece attacks the given piece char after the move has been made.
	// Defaults to king for checking if there's a check, but is also used for other uses.
	virtual bool checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece = 'k');

	// Returns the type of the current piece.
	virtual char getTypeChr(int currColor);
};