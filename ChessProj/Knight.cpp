#include "Knight.h"


bool Knight::validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const
{
	// The knight moves very specific moves, and can jump above other pieces, so no need to check for pieces and only for the
	// coordinates only.
	// The knight's movement is 'L' shaped, so I can substract the piece's coordinates with the new coordinates to check if it creates
	// the 'L' shape.
	if (abs(coords[X_NEW_LOC] - coords[X_LAST_LOC]) == 2 && abs(coords[Y_NEW_LOC] - coords[Y_LAST_LOC]) == 1)
	{
		return true;
	}
	else if (abs(coords[X_NEW_LOC] - coords[X_LAST_LOC]) == 1 && abs(coords[Y_NEW_LOC] - coords[Y_LAST_LOC]) == 2)
	{
		return true;
	}

	return false;
}

bool Knight::checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece)
{
	// Changes the piece the function searches for.
	// By default, the function searches for a king. so if the piece is 'k', whice is the 
	// default, it changes the piece to a black king or white king, according to the current turn color.
	// If it's not 'k', then just leave it as the original piece.
	piece = (piece == 'k') ? (coords[PLAYER_COLOR] == IS_WHITE) ? 'k' : 'K' : piece;

	// All possible moves of a knight, didn't want to write something fancy...
	int x[8] = { 2, 1, -1, -2, -2, -1, 1, 2 };
	int y[8] = { 1, 2, 2, 1, -1, -2, -2, -1 };
	
	// Basically checks all possible combinations of moves for the knight.
	// Checks if it's in the board's borders andif it can get to the piece searched.
	for (int i = 0; i < 8; i++)
	{
		if (coords[Y_NEW_LOC] + y[i] >= 0 && coords[X_NEW_LOC] + x[i] >= 0 &&
			coords[Y_NEW_LOC] + y[i] < BOARD_WIDTH && coords[X_NEW_LOC] + x[i] <= BOARD_WIDTH &&
			board[coords[Y_NEW_LOC] + y[i]][coords[X_NEW_LOC] + x[i]] == piece)
		{
			return true;
		}
	}

	return false;
}

char Knight::getTypeChr(int currColor)
{
	// If the current player color is white, the piece is uppercase, else it's lowercase
	char piece = (currColor == IS_BLACK) ? 'n' : 'N';
	return piece;
}
