#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"


bool Queen::validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const
{
	bool valid = false;

	Rook rook = Rook();
	Bishop bishop = Bishop();

	// The queen's is the rook and bishop combined, so if one of them can move legally to coordinates, the move is legal for the queen.
	if (rook.validStep(coords, board) || bishop.validStep(coords, board))
	{
		valid = true;
	}

	return valid;
}

bool Queen::checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece)
{
	// Changes the piece the function searches for.
	// By default, the function searches for a king. so if the piece is 'k', whice is the 
	// default, it changes the piece to a black king or white king, according to the current turn color.
	// If it's not 'k', then just leave it as the original piece.
	piece = (piece == 'k') ? (coords[PLAYER_COLOR] == IS_WHITE) ? 'k' : 'K' : piece;
	bool chess = false;

	Rook rook = Rook();
	Bishop bishop = Bishop();
	// The queen's is the rook and bishop combined, so if one of them can get legally to the piece, so does the queen.
	if (rook.checkIfInPath(coords, board, piece) || bishop.checkIfInPath(coords, board, piece))
	{
		chess = true;
	}

	return chess;
}

char Queen::getTypeChr(int currColor)
{
	// If the current player color is white, the piece is uppercase, else it's lowercase
	char piece = (currColor == IS_BLACK) ? 'q' : 'Q';
	return piece;
}
