#pragma once
#include <iostream>
#include <vector>
#include "Piece.h"
#include "comm.h"
#include "Bishop.h"
#include "Rook.h"
#include "Pawn.h"
#include "Knight.h"
#include "King.h"
#include "Queen.h"

// Positions of new and old X and Y values in the coords vector
#define X_LAST_LOC 0
#define Y_LAST_LOC 1
#define X_NEW_LOC 2
#define Y_NEW_LOC 3

#define BOARD_EDGE 8
#define CAPITAL_LETTER 32

class Board
{
private:
	std::vector<std::vector<char>> _board;
	bool _isPlayerColorWhite;
	char _lastEatenPiece;

	//Check whitch piece is moved and return 2 error code if there is no piece.
	int switchCaseOfletters(Piece** piece, const std::vector<int>& coords) const;

	// Validates the move in general
	// For example if there's no piece in the chosen coodinates.
	int validateMove(const std::vector<int>& coords) const;

	// Updates the board if the step was successful.
	void updateBoard(const std::vector<int>& coords);

	// Reverts the board to the state before the current move.
	void revertBoard(const std::vector<int>& coords);

	//Check if the coords are valid and in limits.
	bool validCoords(const std::string step);
public:
	// Constructor for the board.
	Board(std::string board, bool startingColor = IS_WHITE);
	
	 /* 
	 The whole turn process:
		- Translates the turn string into an array
		- Check the turn validity
		- Updates the board vector
		- Returns the operation code based on the result of the turn.
	 */
	int resolveTurn(std::string step);

	//print board
	void printBoard() const;

	//board getter.
	std::vector<std::vector<char>> getBoard() const;

	// Player color getter.
	bool isPlayerColorWhite() const;
};