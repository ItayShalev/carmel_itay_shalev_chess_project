#pragma once
#include <vector>
#include <iostream>

// Positions of new and old X and Y values in the coords vector
#define X_LAST_LOC 0
#define Y_LAST_LOC 1
#define X_NEW_LOC 2
#define Y_NEW_LOC 3

#define PLAYER_COLOR 4 // The index of player color in the coords vector
#define IS_WHITE 1
#define IS_BLACK 0
#define EMPTY_SQUARE '#'
#define BOARD_WIDTH 7

#define LAST_SQUARE coords[Y_LAST_LOC]][coords[X_LAST_LOC]
#define NEW_SQUARE coords[Y_NEW_LOC]][coords[X_NEW_LOC]

class Piece
{
public:
	// Checks if the move is valid based on the piece's movement.
	virtual bool validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const = 0;

	// Checks if the piece attacks the given piece char after the move has been made.
	// Defaults to king for checking if there's a check, but is also used for other uses.
	virtual bool checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece = 'k') = 0;

	// Returns the type of the current piece.
	virtual char getTypeChr(int currColor) = 0;
};