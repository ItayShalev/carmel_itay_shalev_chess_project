﻿#include "Bishop.h"

bool Bishop::validStep(const std::vector<int>& coords, const std::vector<std::vector<char>>& board) const
{
	// To check if a line si diagonal (45 degrees from X axis), the formula is:
	// |x1 - x2| = |y1 - y2|.
	// If the 'if' returns false, then the given coordinates don't draw a diagonal line and the move is 
	// illigal forthe bishop.
	if (abs(coords[X_LAST_LOC] - coords[X_NEW_LOC]) != abs(coords[Y_LAST_LOC] - coords[Y_NEW_LOC]))
	{
		return false;
	}

	// Number of squares bishop travels according to the formula:
	// √ [(x2 - x1)² + (y2 - y1)²]
	// Cast to int because no need for float.
	int distance = (int)sqrt(pow(coords[X_NEW_LOC] - coords[X_LAST_LOC], 2) + pow(coords[Y_NEW_LOC] - coords[Y_LAST_LOC], 2));
	// Sets the diagonal line directions, needed to add / subtract from the square coordinates
	// To check if path is clear.
	int right = (coords[X_NEW_LOC] - coords[X_LAST_LOC] > 0) ? 1 : -1; // Should the program increase or decrease x value.
	int up = (coords[Y_NEW_LOC] - coords[Y_LAST_LOC] > 0) ? 1 : -1; // Should the program increase or decrease y value.

	char currPiece = EMPTY_SQUARE;
	// Distance-1 because there's no need to check the last square.
	// i = 1 to skip the square where the bishop is.
	for (int i = 1; i < distance - 1; i++)
	{
		currPiece = board[coords[Y_LAST_LOC] + i * up][coords[X_LAST_LOC] + i * right];
		// If the bishop "collides" with something on the way to the target position.
		if (currPiece != EMPTY_SQUARE)
		{
			return false;
		}
	}

	return true;
}

bool Bishop::checkIfInPath(const std::vector<int>& coords, const std::vector<std::vector<char>>& board, char piece)
{
	// Changes the piece the function searches for.
	// By default, the function searches for a king. so if the piece is 'k', whice is the 
	// default, it changes the piece to a black king or white king, according to the current turn color.
	// If it's not 'k', then just leave it as the original piece.
	piece = (piece == 'k') ? (coords[PLAYER_COLOR] == IS_WHITE) ? 'k' : 'K' : piece;
	int up = 1;
	int right = 1;
	int j = 0;
	int i = 0;
	char currPiece = EMPTY_SQUARE;
	char startPiece = board[NEW_SQUARE];

	// 4 diagonals to check, each time for a different diagonal line.
	for (i = 0; i < 4; i++)
	{
		// J = 1 to skip the square where the piece is.
		// Long condition to make sure coords stay in board borders.
		for (j = 0; coords[X_NEW_LOC] + j * right <= BOARD_WIDTH && coords[Y_NEW_LOC] + j * up <= BOARD_WIDTH && 
			coords[X_NEW_LOC] + j * right >= 0 && coords[Y_NEW_LOC] + j * up >= 0; j++) 
		{
			currPiece = board[coords[Y_NEW_LOC] + j * up][coords[X_NEW_LOC] + j * right];
			if (currPiece == piece)
			{
				return true;
			}
			// I have to start checking from the square where the piece in, so it checks to make sure that loop won't break in that case.
			else if (currPiece != EMPTY_SQUARE && currPiece != startPiece)
			{
				break;
			}
		}
		// The swapping of the parameters that dictate the direction of the diagonal that is checked.
		// In the end, the 4 tests are:
		// up = 1, right = 1
		// up = -1, right = 1
		// up = -1, right = -1
		// up = 1, right = -1
		if (up == 1)
		{
			up = -1;
		}
		else if (right == 1)
		{
			right = -1;
		}
		else
		{
			up = 1;
			right = -1;
		}

	}
	return false;
}

char Bishop::getTypeChr(int currColor)
{
	// If the current player color is white, the piece is uppercase, else it's lowercase
	char piece = (currColor == IS_BLACK) ? 'b' : 'B';
	return piece;
}

