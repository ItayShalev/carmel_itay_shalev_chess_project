#include "comm.h"

std::vector<std::vector<char>> Comm::translateBoard(std::string board)
{
	std::vector<std::vector<char>> boardVec;
	std::vector<char> lineVec;
	int i = 0;
	int j = 0;

	for (i = 0; i < BOARD_EDGES; i++)
	{
		for (j = 0; j < BOARD_EDGES; j++)
		{
			lineVec.push_back(board[i * BOARD_EDGES  + j]);
		}
		boardVec.insert(boardVec.begin(), lineVec);
		lineVec.clear();
	}
	return boardVec;
}

std::vector<int> Comm::translateStep(std::string step, bool playerColor)
{
	std::vector<int> arrStep;
	int loc = 0;
	int asciiSub = 0;
	int i = 0;

	for (i = 0; i < COORDS; i++)
	{
		step[i] = tolower(step[i]); // Works if the letter is upper or lower case.

		// Check if i is even
		// If it is, then the current letter in the step string is the number of the square.
		// If not, then it's the current number of the square.
		asciiSub = (i % 2 == 0) ? ASCII_A : ASCII_1;
		loc = (int)(step[i] - asciiSub);
		arrStep.push_back(loc);
	}

	arrStep.push_back(playerColor);

	return arrStep;
}
