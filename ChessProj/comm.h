#pragma once
#include <iostream>
#include  <vector>

#define ASCII_A 97
#define ASCII_1 49
#define COORDS 4
#define BOARD_EDGES 8

class Comm
{
public:

	// Translates the board string into a 2d array of chars (an 8x8 board).
	static std::vector<std::vector<char>> translateBoard(std::string board);

	// Translates the step string form the frontend into a int[5] array (x0, y0, x1, y1, player color)
	static std::vector<int> translateStep(std::string step, bool playerColor);
};